<?php

namespace Drupal\lifesize_video_call\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Lifesize Video Call' Block.
 *
 * @Block(
 *   id = "lifesize_video_call_block",
 *   admin_label = @Translation("Lifesize Video Call"),
 * )
 */
class LifesizeVideoCallBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['lifesize_video_call_block_name'])) {
      $name = $config['lifesize_video_call_block_name'];
    }
    else {
      $name = $this->t('');
    }
    return array(
      '#markup' => $this->t('<a href="https://call.lifesizecloud.com/extension/@name">Call me on Lifesize!</a>', array(
        '@name' => $name,
      )),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $default_config = \Drupal::config('lifesize.settings');
    $config = $this->getConfiguration();

    $form['lifesize_video_call_block_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Lifesize Video ID'),
      '#description' => $this->t('Please enter your 7 digit Lifesize video ID'),
      '#default_value' => isset($config['lifesize_video_call_block_name']) ? $config['lifesize_video_call_block_name'] : $default_config->get('lifesize.video_id'),
    );

    return $form;
  } 
  
  /**
  * {@inheritdoc}
  */
 public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('lifesize_video_call_block_name', $form_state->getValue('lifesize_video_call_block_name'));
 }
}