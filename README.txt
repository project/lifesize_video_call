CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * License

INTRODUCTION
------------

Current Maintainer: Evan Gipson <egipson@lifesize.com>

The Lifesize Video Call Module provides an easy way to generate an incoming call link for the Lifesize cloud service. You will need to provide your 7-digit Lifesize Video ID in the Block after you drop the Block it into any available container.

Simply fill in your video ID and hit save.

If the Block is provided nothing, no link will show up even if the Block is dragged into a menu.

INSTALLATION
------------

The lifesize plugin is super easy to install! Just follow the instructions below:

1. Copy this lifesize_video_call/ directory to your sites/SITENAME/modules directory.

2. Enable the Lifesize Video Call module in your Drupal admin console.

3. That's it! You should see the Lifesize Video Call Block in your list of available blocks now.

LICENSE
-------

GNU GPL2, see the LICENSE file attached.