# Lifesize Video Call Module
This Module will generate a Block which will contain incoming call link for the Lifesize cloud service. You will need to provide your 7-digit Lifesize Video ID as a module setting after you drop it into any availale container.

For more information, read the README.txt file.